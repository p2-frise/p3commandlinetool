//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

int main (int argc, const char* argv[])
{
    float *floatPoint = nullptr; //declares pointer
    floatPoint = new float[10]; //pointer now points to float on heap
    for (int count = 1; count < 11; count++)
    {
        floatPoint[count] = count;
    }
    for (int count = 1; count < 11; count++)
    {
        std::cout << floatPoint[count] << "\n";
    }
    
    delete[] floatPoint;
    return 0;
}
